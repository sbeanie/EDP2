#ifndef RUNNINGAVERAGE_H
#define RUNNINGAVERAGE_H
#include "ArrayQueue.h"
#include <string>
//---------------
//
//  Running Average class
//  for our EDP 2 project
//
//---------------


class RunningAverage {

    #define CAPACITY 100

    private:
        unsigned short *Q;
        int pSmoothingFactor, pSum;
        ArrayQueue sampleData, averagedData;

    public:

        /**
        ** Constructors for the class
        **/
        RunningAverage(int capacity, int smoothingFactor) : sampleData(smoothingFactor), averagedData(capacity){
		pSmoothingFactor = smoothingFactor;
    		pSum = 0;
	}

        void addSample(unsigned int i);
  	int dequeue();
	std::string getSamples();
	std::string getAverages();
  	int front();
};
#endif
