#ifndef ARRAYQUEUE_H
#define ARRAYQUEUE_H
#include <string>

//---------------
//
//  Array queue class
//  for our EDP 2 project
//
//---------------
class ArrayQueue {

    #define CAPACITY 100

    private:
        unsigned int *Q;
        int pFront, pRear, pSize, n;
        // pFront <- Front index of the queue
        // pRear  <- Rear index of the queue
        // pSize  <- Size of the queue
        // n      <- Capacity of the queue

    public:

        /**
        ** Constructors for the class
        **/
        ArrayQueue();
        ArrayQueue(int capacity);

        // Returns the actual size of the queue
        int size();

        // Checks if the queue is empty
        bool isEmpty();

        // Returns the front element of the queue without dequeue action
        unsigned int front();

        // Enqueues a value
        void enqueue(unsigned int value);

        // Dequeues the first element
        unsigned int dequeue();

    std::string toString();

};
#endif
