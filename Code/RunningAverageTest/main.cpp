#include "mbed.h"
#include "RunningAverage.h"
#include <stdlib.h>
#include <time.h>

BusOut display(p13, p14, p16, p19, p20, p11, p10, p17);
DigitalIn showData(p8);
DigitalIn showSize(p12);
Serial pc(USBTX, USBRX);
DigitalOut led1(LED1);
DigitalOut led2(LED2);

char getHexValue(int i){
    switch(i){
        case 0:
            return 0x3F;
        case 1:
            return 0x06;
        case 2:
            return 0x5B;
        case 3:
            return 0x4F;
        case 4:
            return 0x66;
        case 5:
            return 0x6D;
        case 6:
            return 0x7D;
        case 7:
            return 0x07;
        case 8:
            return 0x7F;
        case 9: 
            return 0x6F;
    }
    return 0x00;        
}

int main() {
    RunningAverage ra(9, 4);
    srand(time(0));
    unsigned int data[9];
    
    wait(1);
    
    /*for(int i = 0; i < 9; i++){
            data[i] = rand() % 10;
            ra.addSample(data[i]); 
            display = getHexValue(data[i]);
            pc.printf("Added data: %d\n", data[i]);
            wait(0.5);  
    }*/
    
    pc.printf("Type 'n' to add a sample, 'd' to dequeue from running average queue, and 'p' to print contents.\n");
    
    while(1){
        char c = pc.getc();
        pc.putc(c);
        if(c == 'n'){
            led1 = 1;
            int random = rand() % 10;
            pc.printf("Adding sample: %d\n", random);
            ra.addSample(random);   
            led1 = 0;
        }else if(c == 'd'){
            led2 = 1;
            pc.printf("Dequeued value: %d\n", ra.dequeue());   
            led2 = 0;
        }else if(c == 'p'){
            pc.printf("%s", ra.getSamples().c_str());
            pc.printf("%s", ra.getAverages().c_str());   
        }
    }
}
 