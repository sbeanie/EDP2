/******************
    Implementation of Running Average
******************/
#include "RunningAverage.h"
#include "ArrayQueue.h"
#include "mbed.h"

void RunningAverage::addSample(unsigned int sample){
    if(sampleData.size() == pSmoothingFactor){
        pSum = pSum - sampleData.dequeue() + sample;
        sampleData.enqueue(sample);
        averagedData.enqueue(int(pSum/pSmoothingFactor));
    }else if(sampleData.size() < pSmoothingFactor - 1){
        sampleData.enqueue(sample);
        pSum = pSum + sample;
    } else if(sampleData.size() == pSmoothingFactor - 1){
        sampleData.enqueue(sample);
        pSum = pSum + sample;
        averagedData.enqueue(int(pSum/pSmoothingFactor));
    }
}

int RunningAverage::dequeue(){
    return averagedData.dequeue();
}

int RunningAverage::size(){
    return averagedData.size();   
}

int RunningAverage::front(){
    return averagedData.front();
}

std::string RunningAverage::getSamples(){
    return sampleData.toString();
}

std::string RunningAverage::getAverages(){
    return averagedData.toString();
}





