/******************
    Has the actual implementation of the queue
******************/
#include "ArrayQueue.h"
#include "mbed.h"

ArrayQueue::ArrayQueue(){
    n = CAPACITY;
    Q = new unsigned int[n];
    pFront = 0;
    pRear = 0;
    pSize = 0;
}

ArrayQueue::ArrayQueue(int capacity){
    n = capacity;
    Q = new unsigned int[n];
    pFront = 0;
    pRear = 0;
    pSize = 0;
}

// Returns the actual size of the queue
int ArrayQueue::size(){
    return pSize;
}

// Checks if the queue is empty
bool ArrayQueue::isEmpty(){
    return pSize==0;
}

int ArrayQueue::getMax(){
	int max = 0;
  	for(int i = 0; i < size(); i++){
  		int index = (pFront + i) % n;
		if(Q[index] > max)max = Q[index];
	}
	return max;
}

int ArrayQueue::getMin(){
	int min = 65535;
  	for(int i = 0; i < size(); i++){
  		int index = (pFront + i) % n;
		if(Q[index] < min)min = Q[index];
	}
	return min;
}

// Returns the front element of the queue without dequeue action
unsigned int ArrayQueue::front(){
    return Q[pFront];
}

// Enqueues a value
void ArrayQueue::enqueue(unsigned int value){
    if(pSize!=n){
        Q[pRear] = value;
        pRear = (pRear+1)%n;
        pSize++;
    }
}

// Dequeues the first element
// Returns 0 if nothing to dequeue
unsigned int ArrayQueue::dequeue(){
    unsigned int dequeuedElement = 0;
    if(!isEmpty()){
        dequeuedElement = Q[pFront];
        Q[pFront] = 0;
        pFront = (pFront+1)%n;
        pSize--;
    }
    return dequeuedElement;
}

