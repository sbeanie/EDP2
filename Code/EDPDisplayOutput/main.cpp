#include "mbed.h"
#include "RunningAverage.h"


/*-----------------------DISPLAY--------------------------*/
#define max7219_reg_noop         0x00
#define max7219_reg_digit0       0x01
#define max7219_reg_digit1       0x02
#define max7219_reg_digit2       0x03
#define max7219_reg_digit3       0x04
#define max7219_reg_digit4       0x05
#define max7219_reg_digit5       0x06
#define max7219_reg_digit6       0x07
#define max7219_reg_digit7       0x08
#define max7219_reg_decodeMode   0x09
#define max7219_reg_intensity    0x0a
#define max7219_reg_scanLimit    0x0b
#define max7219_reg_shutdown     0x0c
#define max7219_reg_displayTest  0x0f

#define LOW 0
#define HIGH 1

SPI max72_spi(PTD2, NC, PTD1);
DigitalOut load(PTD0); //will provide the load signal

/*---------------------------------------------------------*/

//DEBUG
Serial pc(USBTX, USBRX);


AnalogIn wave(PTB0);

Ticker sampleTicker, displayTicker;

static const unsigned char bitReverse8Bit[] = {
  0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0,
  0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, 0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8,
  0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4, 0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4,
  0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, 0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC,
  0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2,
  0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,
  0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6,
  0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE, 0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,
  0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, 0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,
  0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9,
  0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, 0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,
  0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED, 0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,
  0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, 0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3,
  0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, 0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,
  0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, 0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7,
  0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, 0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF
};

unsigned char  pattern_0[8] = {0xF8, 0x88, 0x98, 0xA8, 0xC8, 0x88, 0xF8, 0x00};
unsigned char  pattern_1[8] = {0x80, 0xC0, 0xA0, 0x80, 0x80, 0x80, 0x80, 0x00};
unsigned char  pattern_2[8] = {0x70, 0x88, 0x40, 0x20, 0x10, 0x08, 0xF8, 0x00};
unsigned char  pattern_3[8] = {0xF8, 0x80, 0x80, 0xF8, 0x80, 0x80, 0xF8, 0x00};
unsigned char  pattern_4[8] = {0x88, 0x88, 0x88, 0xF8, 0x80, 0x80, 0x80, 0x00};
unsigned char  pattern_5[8] = {0xF8, 0x08, 0x08, 0xF8, 0x80, 0x80, 0xF8, 0x00};
unsigned char  pattern_6[8] = {0xF8, 0x08, 0x08, 0xF8, 0x88, 0x88, 0xF8, 0x00};
unsigned char  pattern_7[8] = {0xF8, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x00};
unsigned char  pattern_8[8] = {0xF8, 0x88, 0x88, 0xF8, 0x88, 0x88, 0xF8, 0x00};
unsigned char  pattern_9[8] = {0xF8, 0x88, 0x88, 0xF8, 0x80, 0x80, 0x80, 0x00};

static unsigned char* patternFinder[10] = {pattern_0, pattern_1, pattern_2, pattern_3, pattern_4, pattern_5,
	pattern_6, pattern_7, pattern_8, pattern_9};

bool displayArray [8][8];  //Data needs to be added to the displayArray at (displayArrayStart+7) % 8
int displayArrayStart = 0;
unsigned char  outputPattern[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

bool bpmDisplaying = false;


short smoothingFactor = 4;

RunningAverage ra(200, smoothingFactor, 8);


/*
Write to the maxim via SPI
args register and the column data
*/
void write_to_max( int reg, int col)
{
    load = LOW;            // begin
    max72_spi.write(reg);  // specify register
    max72_spi.write(col);  // put data
    load = HIGH;           // make sure data is loaded (on rising edge of LOAD/CS)
}

//writes 8 bytes to the display
void pattern_to_display(unsigned char *testdata){
    int cdata;
    for(int idx = 0; idx <= 7; idx++) {
        cdata = testdata[idx];
        write_to_max(idx+1,cdata);
    }
}


void setup_dot_matrix ()
{
    // initiation of the max 7219
    // SPI setup: 8 bits, mode 0
    max72_spi.format(8, 0);

    max72_spi.frequency(100000); //down to 100khx easier to scope ;-)

    write_to_max(max7219_reg_scanLimit, 0x07);
    write_to_max(max7219_reg_decodeMode, 0x00);  // using an led matrix (not digits)
    write_to_max(max7219_reg_shutdown, 0x01);    // not in shutdown mode
    write_to_max(max7219_reg_displayTest, 0x00); // no display test
    for (int e=1; e<=8; e++) {    // empty registers, turn all LEDs off
        write_to_max(e,0);
    }
   // maxAll(max7219_reg_intensity, 0x0f & 0x0f);    // the first 0x0f is the value you can set
     write_to_max(max7219_reg_intensity,  0x08);
}

void clear(){
     for (int e=1; e<=8; e++) {    // empty registers, turn all LEDs off
        write_to_max(e,0);
    }
}



void sample(){
    unsigned short value = wave.read_u16();
    ra.addSample(value);
}

//Populates the display matrix with data if there's new data to be retrieved.
bool updateDisplayMatrix(){
    unsigned short dequeued = ra.dequeueDivider();
    if(dequeued != 0){
        unsigned short index = 8 * (dequeued - ra.getMin())/(ra.getMax() - ra.getMin());
        if(index >= 8) index = 7;
        index = 7 - index;
        unsigned char columnIndex = (displayArrayStart)% 8;  //had displayArrayStart + 7
        for(char i = 0; i < 8; i++){
            if(i == index)displayArray[columnIndex][i] = 1;
            else displayArray[columnIndex][i] = 0;
        }
        displayArrayStart = (displayArrayStart + 1) % 8;
    }
}


// If not currently showing the BPM it will go through the boolean two dimensional array, creating a char value for each
// row starting from displayArrayStart (circular array queue), and then sending it to the display.
void display(){
    if(!bpmDisplaying){
	if(updateDisplayMatrix()){
            unsigned char sumForRow;
            //Row
            for(int i = 0; i < 8; i++){
                sumForRow = 0;
                //Column
                for(int j = 0; j < 8; j++){
                    //If value is 1 at a particular value it puts a one in the location of the current column
                    if(displayArray[(j + displayArrayStart)% 8][i])sumForRow |= 1 << j;
                }
                unsigned char data = bitReverse8Bit[sumForRow];
                outputPattern[i] = data;
            }
            pattern_to_display(outputPattern);
        }
    }else{
    	short heartbeats = ra.getHeartbeats() * 6;
    	int hundreds = heartbeats / 100;
    	int tens = 0;
    	int ones = heartbeats % 10;
    	if(hundreds > 0){
		tens = (heartbeats /10) % 10;
		pattern_to_display(patternFinder[hundreds]);
        	wait_ms(500);
        	pattern_to_display(patternFinder[tens]);
        	wait_ms(500);
        	pattern_to_display(patternFinder[ones]);
        	wait_ms(500);
    	}else{
    		tens = heartbeats /10;
	        pattern_to_display(patternFinder[tens]);
	        wait_ms(500);
	        pattern_to_display(patternFinder[ones]);
	        wait_ms(500);
    	}
    	bpmDisplaying = false;
    	ra.resetHeartbeats();
    }
}

void setHeartbeat(){
	bpmDisplaying = true;
}


int main(){
    setup_dot_matrix();      /* setup matrix */

    //Makes sure Running Average array queue is populated before main while loop starts
    for(int i = 1; i < smoothingFactor; i++){
        sample();
        wait_ms(1);
    }

    sampleTicker.attach(&sample, 0.01); // setup ticker to call sample every 0.01 seconds (100Hz)
    displayTicker.attach(&setHeartbeat, 10);

    while(true){
        display();
    }
}
