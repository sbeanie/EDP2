/******************
    Implementation of Running Average
******************/
#include "RunningAverage.h"
#include "ArrayQueue.h"
#include "mbed.h"

void RunningAverage::addSample(unsigned short sample){
    if(sampleData.size() == pSmoothingFactor){ //If sampleData is full
        partialAverage = partialAverage + sample/smoothingDivider - (sampleData.dequeue()/smoothingDivider);  //set pSum to the new total
        sampleData.enqueue(sample);                   //enqueue the new sample to sampleData

        int averagedSample = int(partialAverage);
        if(pDividerCounter == pDivider){
                if(averagedData.size() == capacity) dequeue();
        	dividerData.enqueue(averagedSample);
        	pDividerCounter = 0;
        }
        enqueueAveragedData(averagedSample); //enqueue the average of sampleData to averagedData
    }else if(sampleData.size() < pSmoothingFactor - 1){ //If sample data is 2 or more away from being full
        sampleData.enqueue(sample);
        partialAverage = partialAverage + sample/smoothingDivider;
    } else if(sampleData.size() == pSmoothingFactor - 1){ //If sample data is 1 away from being full
        sampleData.enqueue(sample);
        partialAverage = partialAverage + sample/smoothingDivider;
        int averagedSample = int(partialAverage);
        enqueueAveragedData(averagedSample);

        if(pDividerCounter == pDivider){
                if(averagedData.size() == capacity) dequeue();
        	dividerData.enqueue(averagedSample);
        	pDividerCounter = 0;
        }
    }
    pDividerCounter += 1;
}

void RunningAverage::enqueueAveragedData(int sample){
        if(averagedData.size() == capacity) dequeue();
	averagedData.enqueue(sample);
	if(sample > localMax){
		localMax = sample;
		topThreshold = localMin + ((localMax - localMin) *  0.7);
	}
	if(sample < localMin && sample != 0){
		localMin = sample;
		bottomThreshold = localMin + ((localMax - localMin) *  0.3);
	}
	if(topThreshold - bottomThreshold > 1000){
		if(sample >= topThreshold && !topTriggered){
			topTriggered = true;
			heartbeats += 1;
			led1 = false;
			RunningAverage::heartbeatLED.attach(this, &RunningAverage::turnOffLED, 0.1);
		}else if(sample <= bottomThreshold && topTriggered){
			topTriggered = false;
		}
	}
}

int RunningAverage::dequeue(){
    bool scanMax = false, scanMin = false;
    int toDequeue = 0;
    int count = capacity - averagedData.size();

    toDequeue = averagedData.dequeue();
    if(toDequeue == localMax)scanMax = true;
    if(toDequeue == localMin)scanMin = true;

    if(scanMax){
    	localMax = averagedData.getMax();
	topThreshold = localMin + ((localMax - localMin) *  0.8);
    }
    if(scanMin){
    	int tempMin = averagedData.getMin();
    	if(tempMin != 0)localMin = tempMin;
    	bottomThreshold = localMin + ((localMax - localMin) *  0.2);
    }
    return toDequeue;
}

int RunningAverage::getMin(){
    return localMin;
}

int RunningAverage::getMax(){
    return localMax;
}

int RunningAverage::dequeueDivider(){
    return dividerData.dequeue();
}

int RunningAverage::size(){
    return averagedData.size();
}

int RunningAverage::front(){
    return averagedData.front();
}

short RunningAverage::getHeartbeats(){
    return heartbeats;
}

void RunningAverage::resetHeartbeats(){
    heartbeats = 0;
}

void RunningAverage::turnOffLED(){
	led1 = true;
	heartbeatLED.detach();
}
