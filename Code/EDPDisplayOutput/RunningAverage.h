#ifndef RUNNINGAVERAGE_H
#define RUNNINGAVERAGE_H
#include "ArrayQueue.h"
#include "mbed.h"
//---------------
//
//  Running Average class
//  for our EDP 2 project
//
//---------------


class RunningAverage {

    #define CAPACITY 100

    private:
        unsigned short *Q;
        int pSmoothingFactor, pSum, pDividerCounter, pDivider;
        float partialAverage, smoothingDivider;
  	unsigned short heartbeats;
        ArrayQueue sampleData, averagedData, dividerData;
  	DigitalOut led1;
  	bool topTriggered;
  	unsigned short topThreshold, bottomThreshold, localMax, localMin, capacity;
  	void enqueueAveragedData(int sample);
  	Serial pc;
  	Ticker heartbeatLED;

    public:

        /**
        ** Constructors for the class
        **/
        RunningAverage(int capacity, int smoothingFactor, int divider) :pc(USBTX, USBRX),led1(PTB18), sampleData(smoothingFactor), averagedData(capacity), dividerData(divider){
            pDividerCounter = 0;
            pDivider = divider;
            pSmoothingFactor = smoothingFactor;
            pSum = 0;
	    this->capacity = capacity;
            partialAverage = 0;
            smoothingDivider = smoothingFactor;
	    topTriggered = false;
	    heartbeats = 0;
	    topThreshold = 0;
	    bottomThreshold = 0;
	    localMax = 0;
	    localMin = 65535;
    }

        void addSample(unsigned short i);
        int dequeueDivider();
        int dequeue();
        int front();
        int size();
  	int getMin();
  	int getMax();
  	short getHeartbeats();
  	void resetHeartbeats();
    	void turnOffLED();
};
#endif
