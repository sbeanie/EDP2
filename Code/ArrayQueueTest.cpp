#include <stdio.h>
#include "ArrayQueue.h"

int main( int argc, const char* argv[] ){
	ArrayQueue aq (5);
	aq.enqueue(12);
	aq.enqueue(13);
	aq.enqueue(14);
	aq.enqueue(12);
	aq.enqueue(13);
	aq.enqueue(14);
	printf("Front: %d\n",aq.front());
	printf("Dequeue: %d\n", aq.dequeue());
	printf("Dequeue: %d\n", aq.dequeue());
	printf("Dequeue: %d\n", aq.dequeue());
	printf("Dequeue: %d\n", aq.dequeue());
	printf("Dequeue: %d\n", aq.dequeue());
	printf("Dequeue: %d\n", aq.dequeue());
	printf("Dequeue: %d\n", aq.dequeue());
	printf("Dequeue: %d\n", aq.dequeue());

	printf("Created ArrayQueue");
}
